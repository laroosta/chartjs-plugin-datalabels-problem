import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { NavController } from "ionic-angular";
import { Chart } from "chart.js";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage implements OnInit {
  @ViewChild("chartCanvas") private chartCanvas: ElementRef;
  chart: Chart;

  constructor(public navCtrl: NavController) {}

  ngOnInit(): void {
    this.chart = new Chart(this.chartCanvas.nativeElement, {
      type: "line",
      data: {
        labels: ["alpha", "bravo", "charlie", "delta"],
        datasets: [
          {
            data: [2, 10, 6, 12],
            datalabels: {
              color: "#36A2EB"
            }
          }
        ]
      }
    });
  }
}
